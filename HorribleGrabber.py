import sys
from qbittorrent import Client
import requests
import threading
import time
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget,\
    QPushButton, QHBoxLayout, QLineEdit, QLabel, QListWidget, QListWidgetItem, QAbstractItemView


qb = Client('http://127.0.0.1:8080/')
searchkeysH = []
Stop = True
Active = False


def getHorFeed():
    listlink = []
    listtitle = []

    adr = 'http://www.horriblesubs.info/rss.php?res=720'

    try:
        r = requests.get(adr)

    except Exception as e:
        print(e)

    res = r.content
    a = res.decode()

    while True:
        begin = a.find('<link>')
        if begin < 0:
            break
        end = a.find('</link>')
        if end < 0:
            break
        listlink.append(a[begin+6:end])
        a = a[end+6:-1]

    a = res.decode()
    while True:
        begin = a.find('<title>')
        if begin < 0:
            break
        end = a.find('</title>')
        if end < 0:
            break
        listtitle.append(a[begin+7:end])
        a = a[end+7:-1]
    return listtitle, listlink


def scrape_run():
    global Active
    qb.login('admin', '2Pussy69')
    titles, links = getHorFeed()
    torrents = qb.torrents()
    for key in searchkeysH:
        inner = 0
        for i in range(0, len(titles)):
            c = titles[i].find(key)
            if c != -1:
                for torrent in torrents:
                    if torrent['name'] == titles[i]:
                        inner = -1
                        break
                if inner == 0:
                    qb.download_from_link(links[i])
    Active = False


def scrape_run_a():
    global Active
    while not Stop:
        qb.login('admin', '2Pussy69')
        titles, links = getHorFeed()
        torrents = qb.torrents()
        for key in searchkeysH:
            inner = 0
            for i in range(0, len(titles)):
                c = titles[i].find(key)
                if c != -1:
                    for torrent in torrents:
                        if torrent['name'] == titles[i]:
                            inner = -1
                            break
                    if inner == 0:
                        print("Found:"+torrent['name'])
                        qb.download_from_link(links[i])
        print("Search Complete")
        for i in range(0, 10800):
            if Stop:
                break
            else:
                time.sleep(1)

    Active = False


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.main_widget = QWidget(self)
        self.search = ''
        self.thread = []
        # #create layout
        hbox1 = QHBoxLayout()
        hbox1.addStretch(1)
        hbox3 = QHBoxLayout()
        hbox3.addStretch(1)
        hbox4 = QHBoxLayout()
        hbox4.addStretch(1)
        hbox5 = QHBoxLayout()
        hbox5.addStretch(1)
        vbox1 = QVBoxLayout()
        vbox1.addStretch(1)

        # #create run button
        self.btn1 = QPushButton('Start', self)
        self.btn1.clicked.connect(self.start)
        self.btn1.resize(self.btn1.sizeHint())
        self.btn1.setToolTip('Perform a single scrapping based on given search list')

        # #create add button
        self.btn2 = QPushButton('Add', self)
        self.btn2.clicked.connect(self.add)
        self.btn2.resize(self.btn1.sizeHint())
        self.btn2.setToolTip('Add item to search list')

        # #create auto button
        self.btn3 = QPushButton('Auto', self)
        self.btn3.clicked.connect(self.auto)
        self.btn3.resize(self.btn1.sizeHint())
        self.btn3.setToolTip('Perform scrapping based on given search list every hour')

        # #create stop button
        self.btn4 = QPushButton('Stop', self)
        self.btn4.clicked.connect(self.stop)
        self.btn4.resize(self.btn1.sizeHint())
        self.btn4.setToolTip('Stop current operation')

        # #create delete button
        self.btn5 = QPushButton('Delete', self)
        self.btn5.clicked.connect(self.delete)
        self.btn5.resize(self.btn1.sizeHint())
        self.btn5.setToolTip('Delete selected items from list')

        # #create search topic box
        self.lbl2 = QLabel()
        self.lbl2.setText('Anime name')
        self.top = QLineEdit(self)
        self.top.textChanged[str].connect(self.on_changed)

        # #create list box for search results
        self.list = QListWidget()
        self.list.setSelectionMode(QAbstractItemView.ExtendedSelection)

        # #feed widgets into layout
        hbox3.addWidget(self.lbl2)
        hbox3.addWidget(self.top)
        vbox1.addLayout(hbox3)
        hbox4.addWidget(self.btn1)
        hbox4.addWidget(self.btn2)
        hbox4.addWidget(self.btn3)
        hbox4.addWidget(self.btn4)
        hbox5.addWidget(self.btn5)
        vbox1.addLayout(hbox4)
        vbox1.addLayout(hbox5)
        vbox1.addWidget(self.list)
        hbox1.addLayout(vbox1)

        self.main_widget.setLayout(hbox1)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.initui()

    def initui(self):
        self.setWindowTitle('Horriblesubs Anime Grabber')
        self.show()

    # #Function for 'run' button
    def start(self):
        global Stop, Active
        if not Active:
            Active = True
            Stop = False
            self.thread = threading.Thread(target=scrape_run, args=())
            self.thread.daemon = True
            self.thread.start()

    # #Function for 'add' button
    def add(self):
        global searchkeysH
        searchkeysH.append(self.search)
        item = QListWidgetItem(self.search)
        self.list.addItem(item)

    # #Function for 'auto' button
    def auto(self):
        global Stop, Active
        if not Active:
            Active = True
            Stop = False
            self.thread = threading.Thread(target=scrape_run_a, args=())
            self.thread.daemon = True
            self.thread.start()

    # #Function for 'stop' button
    def stop(self):
        global Stop
        Stop = True

    # #Function for 'delete' button
    def delete(self):
        global searchkeysH
        listit = self.list.selectedItems()
        for item in listit:
            temp = item.text()
            searchkeysH.remove(temp)
            self.list.takeItem(self.list.row(item))

    # #Function for reading in topic when edited
    def on_changed(self, text):
        self.search = text


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

