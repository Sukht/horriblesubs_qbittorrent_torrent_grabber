import sys
from qbittorrent import Client
import requests
import threading
from PyQt5.QtCore import Qt
from bs4 import BeautifulSoup as bs
import time
import datetime
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget,\
    QPushButton, QHBoxLayout, QListWidget, QListWidgetItem, QAbstractItemView

qb = Client('http://127.0.0.1:8080/')
Stop = True
Active = False


def schedule():
    sch = []
    # get data
    data = requests.get('https://horriblesubs.info/release-schedule/')

    # load data to bs4
    soup = bs(data.text, 'html.parser')

    current = 0
    for table in soup.find_all('table', {'class': 'schedule-today-table'}):
        for tr in table.find_all('tr', {'class': 'schedule-page-item'}):
            title = tr.find('a').text
            times = tr.find('td', {'class': 'schedule-time'}).text
            sch.append((title, current, times))
        if current == 6:
            break
        current += 1

    for kk in range(0, len(sch)-1):
        for zz in range(kk+1, len(sch)):
            if sch[kk][0] > sch[zz][0]:
                a = sch[kk]
                sch[kk] = sch[zz]
                sch[zz] = a

    return sch


def get_hor_feed():
    listlink = []
    listtitle = []

    adr = 'http://www.horriblesubs.info/rss.php?res=720'

    try:
        r = requests.get(adr)

    except Exception as e:
        print(e)

    res = r.content
    a = res.decode()

    while True:
        begin = a.find('<link>')
        if begin < 0:
            break
        end = a.find('</link>')
        if end < 0:
            break
        listlink.append(a[begin+6:end])
        a = a[end+6:-1]

    a = res.decode()
    while True:
        begin = a.find('<title>')
        if begin < 0:
            break
        end = a.find('</title>')
        if end < 0:
            break
        listtitle.append(a[begin+7:end])
        a = a[end+7:-1]
    return listtitle, listlink


def get_desc(title):
    title2 = title.replace(' ', '+')
    a = True
    try:
        data = requests.get('https://horriblesubs.info/shows/{}/'.format(title2))
        soup = bs(data.text, 'html.parser')
    except Exception as e:
        a = False
        print(e)
    if a:
        data = requests.get('https://horriblesubs.info/shows/{}/'.format(title))
        soup = bs(data.text, 'html.parser')
        for desc in soup.find_all('div', {'class': 'series-desc'}):
            for disc in desc.find('p'):
                return str(disc).replace('. ', '.\n')

    else:
        return 'No Description'


def scrape_run(name, day, timer):
    qb.login('admin', '2Pussy69')
    temp = datetime.datetime.utcnow()
    temp1 = str(temp.time()).split(':')
    temp1d = temp.weekday()
    temp2 = 24*60*float(temp1d)+60*float(temp1[0]) + float(temp1[1])
    temp1 = timer.split(':')
    exp = 24*60*float(day)+60*(float(temp1[0])+8.0) + float(temp1[1])
    wait = exp - temp2
    if wait < -180:
        wait += 7*24*60
    if wait > 0:
        time.sleep(wait*60)

    found = False
    for km in range(1, 4):
        titles, links = get_hor_feed()
        torrents = qb.torrents()
        for kmm in range(0, len(titles)):
            c = titles[kmm].find(name)
            inner = 0
            if c != -1:
                for torrent in torrents:
                    if torrent['name'] == titles[kmm]:
                        inner = -1
                        break
                if inner == 0:
                    qb.download_from_link(links[kmm])
                    print(titles[kmm])
                    found = True
        if found:
            break


def scrape_run_a(name, day, timer):
    qb.login('admin', '2Pussy69')
    temp = datetime.datetime.utcnow()
    temp1 = str(temp.time()).split(':')
    temp1d = temp.weekday()
    temp2 = 24 * 60 * float(temp1d) + 60 * float(temp1[0]) + float(temp1[1])
    temp1 = timer.split(':')
    exp = 24 * 60 * float(day) + 60 * (float(temp1[0]) + 8.0) + float(temp1[1])
    wait = exp - temp2
    if wait < -180:
        wait += 7 * 24 * 60
    if wait > 0:
        time.sleep(wait * 60)

    found = False
    for km in range(1, 4):
        titles, links = get_hor_feed()
        torrents = qb.torrents()
        for kmm in range(0, len(titles)):
            c = titles[kmm].find(name)
            inner = 0
            if c != -1:
                for torrent in torrents:
                    if torrent['name'] == titles[kmm]:
                        inner = -1
                        break
                if inner == 0:
                    qb.download_from_link(links[kmm])
                    print(titles[kmm])
                    found = True
        if found:
            break
        else:
            time.sleep(3600)

    while not Stop:
        temp = datetime.datetime.utcnow()
        temp1 = str(temp.time()).split(':')
        temp1d = temp.weekday()
        temp2 = 24 * 60 * float(temp1d) + 60 * float(temp1[0]) + float(temp1[1])
        wait = exp - temp2 + 24*7*60
        time.sleep(wait * 60)
        found = False
        for km in range(1, 4):
            titles, links = get_hor_feed()
            torrents = qb.torrents()
            for kmm in range(0, len(titles)):
                c = titles[kmm].find(name)
                inner = 0
                if c != -1:
                    for torrent in torrents:
                        if torrent['name'] == titles[kmm]:
                            inner = -1
                            break
                    if inner == 0:
                        qb.download_from_link(links[kmm])
                        print(titles[kmm])
                        found = True
            if found:
                break
            else:
                time.sleep(3600)


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.main_widget = QWidget(self)
        self.search = ''
        self.thread = []
        self.sched = schedule()
        # #create layout
        hbox1 = QHBoxLayout()
        hbox1.addStretch(1)
        hbox3 = QHBoxLayout()
        hbox3.addStretch(1)
        hbox4 = QHBoxLayout()
        hbox4.addStretch(1)
        hbox5 = QHBoxLayout()
        hbox5.addStretch(1)
        vbox1 = QVBoxLayout()
        vbox1.addStretch(1)

        # #create run button
        self.btn1 = QPushButton('Start', self)
        self.btn1.clicked.connect(self.start)
        self.btn1.resize(self.btn1.sizeHint())
        self.btn1.setToolTip('Perform a single scrapping based on given search list')

        # #create auto button
        self.btn3 = QPushButton('Auto', self)
        self.btn3.clicked.connect(self.auto)
        self.btn3.resize(self.btn1.sizeHint())
        self.btn3.setToolTip('Perform scrapping based on given search list every hour')

        # #create stop button
        self.btn4 = QPushButton('Stop', self)
        self.btn4.clicked.connect(self.stop)
        self.btn4.resize(self.btn1.sizeHint())
        self.btn4.setToolTip('Stop current operation')

        # #create delete button
        self.btn5 = QPushButton('Update', self)
        self.btn5.clicked.connect(self.updateer)
        self.btn5.resize(self.btn1.sizeHint())
        self.btn5.setToolTip('Delete selected items from list')

        # #create list box for search results
        self.list = QListWidget()
        self.list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        for show in self.sched:
            item = QListWidgetItem(show[0])
            item.setData(Qt.UserRole, show)
            # print(show[0])
            item.setToolTip(get_desc(show[0]))
            self.list.addItem(item)

        # #feed widgets into layout
        vbox1.addLayout(hbox3)
        hbox4.addWidget(self.btn1)
        hbox4.addWidget(self.btn3)
        hbox4.addWidget(self.btn4)
        hbox5.addWidget(self.btn5)
        vbox1.addLayout(hbox4)
        vbox1.addLayout(hbox5)
        vbox1.addWidget(self.list)
        hbox1.addLayout(vbox1)

        self.main_widget.setLayout(hbox1)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.initui()

    def initui(self):
        self.setWindowTitle('Horriblesubs Anime Grabber')
        self.show()

    # #Function for 'run' button
    def start(self):
        global Stop
        Stop = False
        listit = self.list.selectedItems()
        for item in listit:
            thready = threading.Thread(target=scrape_run, args=item.data(Qt.UserRole))
            self.thread.append(thready)
        for thread in self.thread:
            thread.daemon = True
            thread.start()

    # #Function for 'auto' button
    def auto(self):
        global Stop
        Stop = False
        listit = self.list.selectedItems()
        for item in listit:
            thready = threading.Thread(target=scrape_run_a, args=item.data(Qt.UserRole))
            self.thread.append(thready)
        for thread in self.thread:
            thread.daemon = True
            thread.start()

    # #Function for 'stop' button
    def stop(self):
        global Stop
        Stop = True

    # #Function for 'delete' button
    def delete(self):
        listit = self.list.selectedItems()
        for item in listit:
            temp = item.text()
            self.list.takeItem(self.list.row(item))

    def updateer(self):
        self.list.clear()
        self.sched = schedule()
        for show in self.sched:
            item = QListWidgetItem(show[0])
            item.setData(Qt.UserRole, show)
            self.list.addItem(item)

    # #Function for reading in topic when edited
    def on_changed(self, text):
        self.search = text


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

